﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    bool Move = false; // move is true when the player is moving
    public bool IsGrounded = false; // is grounded is true when the player is on the ground

    public Rigidbody2D rb; // the player rigidbody var

    public float Speed = 20f; // normal speed var
    public float RotationSpeed = 2f; // rotation speed var


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) // if the user presses the space bar
        {
            Move = true; // set the move to true
        }
        if (Input.GetKeyUp(KeyCode.Space)) // if the player stops pressing the space bar
        {
            Move = false; // set the move to false
        }
    }

    /// <summary>
    /// we are using a fixed update method to delay all physics related running codes. 
    /// </summary>
    private void FixedUpdate()
    {
        if (Move) // if move is true
        {
            if (IsGrounded) // and if the player is on grounded
            {
                rb.AddForce(transform.right * Speed * Time.fixedDeltaTime * 100f, ForceMode2D.Force); // adding moving force to the player, by moving him forward
            }
            else // if the player is not on the ground
            {
                rb.AddTorque(RotationSpeed * Time.fixedDeltaTime * 100f, ForceMode2D.Force); // adding a rotational force to the player, to make him spin in the air
            }
        }
    }

    /// <summary>
    /// on collision enter is called when two objects with colliders are meeting each other, after the collision, the function can be called via script, and giving to one of the objects a full information about the other object
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Line")) // if the object that the car collided with is a line
        {
            IsGrounded = true; // set is grounded to true
        }
    }

    /// <summary>
    /// on coliision exit is similar to on collision enter, only that it can be called when two objects stop colliding. 
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Line")) // if the object that the car stopped colliding with is a line
        {
            IsGrounded = false; // set is grounded to false
        }

    }

}
