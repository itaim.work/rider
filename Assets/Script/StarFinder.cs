﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarFinder : MonoBehaviour
{
    public GameManager GameManagerScript; // game manager script var

    /// <summary>
    /// on trigger enter 2D is a function that can be called when an object with a collider that is marked as a trigger is colliding with
    /// another object with a normal collider
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player")) // if the player collided with the star
        {
            GameManagerScript.AddScore(); // call the addScore function in the game manager

            Destroy(gameObject); // destroy the star
        }
    }

}
