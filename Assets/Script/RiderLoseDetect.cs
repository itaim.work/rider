﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiderLoseDetect : MonoBehaviour
{
    public GameManager GameManagerScript; // game manager script var

    private void Update()
    {
        if(transform.position.y <= -10) // if the player y axis position is below -10
        {
            GameManagerScript.PlayerLost(); // send a playerLost function call to the game manager script
        }
    }


    /// <summary>
    /// on trigger enter 2D is a function that can be called when an object with a collider that is marked as a trigger is colliding with
    /// another object with a normal collider
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.CompareTag("Line")) // if the collided object is the line
        {
            GameManagerScript.PlayerLost(); // send a playerLost function call to the game manager script
        }

    }

}
