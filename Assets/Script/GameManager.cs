﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject PauseMenuUI; // pause menu UI var

    public GameObject LoseMenuUI; // lose menu UI var

    public GameObject WinMenuUI; // win menu UI var

    public bool CantPause = false; // a meothed to disable pause menu when finished level or lost the level

    public Text ScoreText; // the score text var

    public int Score; // the score info var

    private void Start()
    {
        CantPause = false; // make sure the cantPause bool is set to false to make sure that the pause menu can open
        Time.timeScale = 1;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && !CantPause) // if the user is pressing the "P" key and the game can be paused
        {
            PauseLevel(); // call the pause level function
        }

        if (Input.GetKeyDown(KeyCode.R) && !CantPause) // if the user is pressing the "R" key and the game can be paused
        {
            ResumeLevel(); // call the resume level function
        }

        ScoreText.text = "Score: " + Score; // display the score amount on the screen
    }

    /// <summary>
    /// add score function
    /// </summary>
    public void AddScore()
    {
        Score += 50; // add a 50 to the score count
    }

    /// <summary>
    /// Resume Level function
    /// </summary>
    public void ResumeLevel()
    {
        Time.timeScale = 1; // set the time scale to 1 to resume the game cycle

        PauseMenuUI.SetActive(false); // turn off the pause menu UI
    }

    /// <summary>
    /// pause level function
    /// </summary>
    public void PauseLevel()
    {
        Time.timeScale = 0; // set the time scale to 0 to pause the game cycle

        PauseMenuUI.SetActive(true); // turn on the pause menu UI
    }

    /// <summary>
    /// restart level function
    /// </summary>
    public void RestartLevel()
    {
        Time.timeScale = 1; // set the time scale to 1 to resume the game cycle

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // call the scene manager to load the running scene
    }

    /// <summary>
    /// main menu function
    /// </summary>
    public void MainMenu()
    {
        Time.timeScale = 1; // set the time scale to 1 to resume the game cycle

        SceneManager.LoadScene(0); // call the scene manager to load the main menu
    }

    /// <summary>
    /// player lost function
    /// </summary>
    public void PlayerLost()
    {
        CantPause = true; // set cantPause to true

        Time.timeScale = 0; // set the time scale to 0 to pause the game cycle

        LoseMenuUI.SetActive(true); // turn on the lose menu UI
    }

    /// <summary>
    /// player won function
    /// </summary>
    public void PlayerWon()
    {
        CantPause = true; // set cantPause to true

        Time.timeScale = 0; // set the time scale to 0 to pause the game cycle

        WinMenuUI.SetActive(true); // turn on the win menu UI
    }

    /// <summary>
    /// next level function
    /// </summary>
    public void NextLevel()
    {
        Time.timeScale = 1; // set the time scale to 1 to resume the game cycle

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // call the scene manager to load the next scene in the build line
    }
}
