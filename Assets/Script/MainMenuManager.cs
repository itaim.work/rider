﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public GameObject MainMenuUI; // main menu UI var

    public GameObject LevelMenuUI; // levels menu UI var

    /// <summary>
    /// open level menu function
    /// </summary>
    public void OpenLevelMenu()
    {
        MainMenuUI.SetActive(false); // turn off the main menu UI

        LevelMenuUI.SetActive(true); // turn on the level menu UI
    }

    /// <summary>
    /// close level menu function
    /// </summary>
    public void CloseLevelMenu()
    {
        MainMenuUI.SetActive(true); // turn on the main menu UI

        LevelMenuUI.SetActive(false); // turn off the level menu UI

    }


    /// <summary>
    /// close game function
    /// </summary>
    public void QuitGame()
    {
        Application.Quit(); // call the app to close
    }

    /// <summary>
    /// play level function that needs a level index to operate
    /// </summary>
    /// <param name="LevelIndex"></param>
    public void PlayLevel(int LevelIndex)
    {
        SceneManager.LoadScene(LevelIndex); // using the level index to call the correct level in the scene manager
    }

}
