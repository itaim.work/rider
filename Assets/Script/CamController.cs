﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamController : MonoBehaviour
{
    public Transform Target; // a target var for the camera follow

    // Update is called once per frame
    void Update()
    {
         transform.position = new Vector3(Target.position.x, Target.position.y, transform.position.z); // ressting the camera poistion over the target x position
    }
}
